//
//  MenuViewController.swift
//  NewViews
//
//  Created by Wayne Hoit on 12/24/18.
//  Copyright © 2018 Wayne Hoit. All rights reserved.
//

import UIKit

var newVC:String = ""
var MenuNav: UINavigationController!

class MenuViewController: UIViewController{
    
    @IBOutlet weak var Item1_But: UIButton!
    @IBOutlet weak var Item2_But: UIButton!
    @IBOutlet weak var Item3_But: UIButton!
    @IBOutlet weak var Item4_But: UIButton!
    @IBOutlet weak var Item5_But: UIButton!
    @IBOutlet weak var Item6_But: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        MenuNav = self.navigationController!
    }
    
    @IBAction func Item1_Tap(_ sender: UIButton) {
        newVC = "Item1"
        handlePush()
    }
    @IBAction func Item2_Tap(_ sender: UIButton) {
        newVC = "Item2"
        handlePush()
    }
    @IBAction func Item3_Tap(_ sender: UIButton) {
        newVC = "Item3"
        handlePush()
    }
    @IBAction func Item4_Tap(_ sender: UIButton) {
        newVC = "Item4"
        handlePush()
    }
    @IBAction func Item5_Tap(_ sender: UIButton) {
        newVC = "Item5"
        handlePush()
    }
    @IBAction func Item6_Tap(_ sender: UIButton) {
        newVC = "Item6"
        handlePush()
    }
    
    func handlePush(){
        var topVC: UIViewController!
        var nextVC: UIViewController!
        topVC = self.storyboard?.instantiateViewController(withIdentifier: "TopViewController") as! TopViewController
        topVC.loadViewIfNeeded()
        
        switch newVC{
            case "Item1","Item2":
                if (newVC == "Item1"){
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item1") as! Item1
                }else{
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item2") as! Item2
                }
                TopNav.pushViewController(nextVC, animated: false)
            case "Item3","Item4","Item5","Item6":
                let botVC = self.storyboard?.instantiateViewController(withIdentifier: "BotViewController") as! BotViewController
                botVC.loadViewIfNeeded()
                if (newVC == "Item3"){
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item3") as! Item3
                }else if (newVC == "Item4"){
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item4") as! Item4
                }else if (newVC == "Item5"){
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item5") as! Item5
                }else if (newVC == "Item6"){
                    nextVC = storyboard?.instantiateViewController(withIdentifier: "Item6") as! Item6
                }
                BotNav.pushViewController(nextVC, animated: false)
                TopNav.pushViewController(botVC, animated: false)
            default:
                return
        }
        MenuNav.pushViewController(topVC, animated: true)
    }
}
