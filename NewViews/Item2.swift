//
//  Item2.swift
//  NewViews
//
//  Created by Wayne Hoit on 12/24/18.
//  Copyright © 2018 Wayne Hoit. All rights reserved.
//

import UIKit

class Item2: UIViewController {
    @IBOutlet weak var But_Go2A: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func But_Go2A_Tap(_ sender: UIButton) {
        // Special case to load BotView (with BotNav) with Item2A embedded
        let botVC = self.storyboard?.instantiateViewController(withIdentifier: "BotViewController") as! BotViewController
        botVC.loadViewIfNeeded()
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "Item2A") as! Item2A
        BotNav.pushViewController(nextVC, animated: false)
        TopNav.pushViewController(botVC, animated: true)
    }
}
