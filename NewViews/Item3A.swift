//
//  Item3A.swift
//  NewViews
//
//  Created by Wayne Hoit on 12/24/18.
//  Copyright © 2018 Wayne Hoit. All rights reserved.
//

import UIKit

class Item3A: UIViewController {
    @IBOutlet weak var But_Go3B: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func But_Go3B_Tap(_ sender: UIButton) {
        //let junk = self.navigationController?.restorationIdentifier
        //print("Item3A parent nav: \(junk!)")
        let toVC = storyboard?.instantiateViewController(withIdentifier: "Item3B") as! Item3B
        BotNav.pushViewController(toVC, animated: true)
    }
}
