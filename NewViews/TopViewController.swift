//
//  TopViewController.swift
//  NewViews
//
//  Created by Wayne Hoit on 12/24/18.
//  Copyright © 2018 Wayne Hoit. All rights reserved.
//

import UIKit

var TopNav: UINavigationController!

class TopViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func backButtonTapped() {
        if BotNav != nil {
            if (BotNav.viewControllers.count == 1) && (TopNav.viewControllers.count == 2) {
                // Special case for Item2 which starts only in TopNav (count=1) then pushes BotNav(topnav count=2) containing item2A
                TopNav.popViewController(animated: true)
            }else if (BotNav.viewControllers.count == 1) {
                MenuNav.popViewController(animated: true)
            }else if (BotNav.viewControllers.count > 1) {
                BotNav.popViewController(animated: true)
            }
        }else if (TopNav.viewControllers.count == 1) {
            MenuNav.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        TopNav = segue.destination as? UINavigationController
    }
}
